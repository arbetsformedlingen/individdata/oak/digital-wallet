import entext from './locales/en';
import svtext from './locales/sv';

export const LOCALES = {
  en: entext,
  sv: svtext,
};

export default LOCALES;
