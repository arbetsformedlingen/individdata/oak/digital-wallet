/* eslint-disable max-len */
export default {
  landingpage_title: 'Dela din data med ',
  landingpage_line1: 'Påbörja din dataöverföring genom att identifiera dig nedan.',
  landingpage_line2: 'Ett användarkonto skapas automatiskt när du loggar in första gången.',
  landingpage_link: 'Project OAK allmänna villkor',
  landingpage_footer: 'Project OAK är en myndighetstjänst som gör det möjligt att lagra och skicka sin egna data mellan statliga och privata aktörer.',
  login_button: 'Logga in',
  log_out_button: 'Logga ut',
  share_later_button: 'Dela senare',
  next_button: 'Nästa',
  close_button: 'Stäng',
  get_data_button: 'Hämta data',
  view_and_share_data_button: 'Granska och dela data',
  continue_to_get_data_button: 'Fortsätt',
  continue_to_share_data_button: 'Fortsätt',
  return_to_requesting_service_button: 'Gå tillbaka till ursprungstjänsten',
  consent_and_get_data_button: 'Godkänn och hämta data',
  get_your_data_text: 'Hämta data',
  share_your_data_text: 'Dela data',
  log_in_text: 'Logga in',
  home_page_link: 'Hur loggar jag in med Freja eID?',
  image_page_title: 'Din data i din kontroll',
  image_page_description: 'Project OAK är en myndighetstjänst som gör det möjligt att lagra och skicka sin egna data mellan statliga och privata aktörer.',
  inbox_text: 'Inkorg',
  consents_text: 'Consents',
  my_data_text: 'My data',
  get_from_text: '(sv) Get from: ',
  share_with_text: 'Dela med: ',
  first_get_your_document_text: '1. Hämta dokument',
  second_view_and_share_your_document_text: '2. Granska och dela dokument',
  share_your_text: 'Dela ',
  with_text: 'med ',
  popup_consent_title: 'Godkänn överföring',
  popup_consent_subtitle: 'Hämta Arbetslöshetsintyg från Arbetsförmedlingen.',
  popup_fetch_data_text: 'Hämtar data...',
  popup_share_data_text: 'Delar data...',
  popup_check_get_data_info_text: '(sv) BNP Paribas will be provided information that you are registered as a job seeker in Arbetsförmedlingen and the date of such registration. For privacy reasons, BNP Paribas will not have access to any other information about you. You can withdraw your consent at any time which will terminate any further data transfer. When you are no longer registered as a job seeker in Arbetsförmedlingen, the transfer automatically terminates and you will need to provide an additional consent for the "job-seeker status" final date to be transfered to BNP Paribas.',
  popup_check_get_data_text_1: '(sv) I give my consent for the Project OAK to handle and transfer this information once to BNP Paribas.',
  popup_check_get_data_text_2: '(sv) I agree that this process removes the confidentiality of the information I choose to share with BNP Paribas.',
  popup_check_get_data_text_3: '(sv) I have reviewed the information and confirm that no unwanted information is included in the transfer to Dummy.',
  popup_under_proceed_text: '(sv) Your fetching is under proceed get notified when it&apos;s ready.',
  popup_success_share_title: '(sv) Your unemployement certificate is now being shared with BNP Paribas.',
  popup_success_share_subtitle: '(sv) You can always revoke your consent under Consents.',
  popup_success_share_description: '(sv) Stay up to date on all your data matters.',
  popup_success_share_sub_description: '(sv) Just enter your Email address',
  popup_review_share_data_title: '(sv) Review your document data',
  popup_review_share_data_subtitle: '(sv) Review your Unemployment certificate data to be shared with BNP Paribas.',
  popup_success_fetch_data_text: '(sv) Your Registration certificate from Arbetsförmedlingen has now been fetched. Click view data to review all the fetched data.',
};
