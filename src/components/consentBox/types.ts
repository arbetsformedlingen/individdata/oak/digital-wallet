export type People = {
  status: string;
};

export interface ButtonProps {
  id: string | undefined;
  status: string;
  providerWebId: string,
}
