export type Title = {
  status: string;
};

export type Logo = {
  number: string;
  status: string;
};

export type Text = {
  id: string;
};

export type Arrow = {
  status: string;
};
