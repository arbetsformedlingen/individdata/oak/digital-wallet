export const style2 = {
  whiteBox: {
    color: 'white',
    display: 'flex',
    justifyContent: 'center',
    textAlign: 'center',
    height: '680px',
  } as any,
  h: {
    height: '15%',
  },
};

export const style5 = {
  marginTop: {
    marginTop: '50px',
    padding: '20px',
  },
  fonts: {
    fontSize: '15px',
    fontWeight: '600',
  },
  buttonS: {
    width: '80%',
    height: '54px',
    background: '#65D36E',
    borderRadius: '46px',
    minWidth: '117px',
  },
  center: {
    display: 'flex', justifyContent: 'center', alignItems: 'center',

  },
  inActive: {
    minWidth: '117px',
    width: '80%',
    height: '54px',
    background: 'transparent',
    border: ' 2px solid #65D36E',
    color: '#65d36e',
    borderRadius: '46px',
  },
  hide: {
    minWidth: '117px',
    width: '80%',
    height: '54px',
    background: 'transparent',
    border: ' 2px solid #ACACAC',
    color: '#ACACAC',
    borderRadius: '46px',

  },

  active: {
    minWidth: '117px',
    width: '80%',
    height: '54px',
    background: '#65D36E',
    borderRadius: '46px',
  },

  button: {
    width: '60%',
    height: '60%',
    marginTop: '10px',
    marginBottom: '40px',
    background: '#5cbc64',
    borderRadius: '24px',
    border: 'none',
  },

  buttonText: {
    width: '100%',
    fontFamily: 'Open Sans',
    fontStyle: 'normal',
    fontWeight: '600',
    fontSize: '16px',
    textAlign: 'center',
    color: '#1a1a1a',
  },
};

export const style3 = {
  backgroundColor: 'black',
  borderRadius: '15px',
};

export const style4 = {
  pad10: {
    padding: '10px', justifyContent: 'center', marginTop: '20px',
  },
  pad20: {
    padding: '10px', justifyContent: 'center',
  },
  centerRow: {
    display: 'flex', flexDirection: 'row', justifyContent: 'center',
  },
  leftRow: {
    display: 'flex', flexDirection: 'row',
  },
  colorBottom: {
    color: '#65D36E',
    marginBottom: '10px',
  },
  minColor: {
    color: '#65D36E',
    minWidth: '110px',
  },

  center: {
    display: 'flex',
    justifyContent: 'center',
    textAlign: 'center',

  },
  danger: {
    fontSize: '35px',
    color: '#FFFF00',
  },
};
