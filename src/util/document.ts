/* eslint max-len: ["error", { "ignoreStrings": true }] */
export const reviewGetdataTitle1 = 'Preview of your document transfer';
export const reviewGetdataTitle2 = 'You are about to fetch your Unemployment certificate from Arbetsförmedlingen.';
export const checkGetdataTitle1 = 'Consent document transfer';
export const checkGetdataTitle2 = 'You are about to fetch your Unemployment certificate from Arbetsförmedlingen.';
export const reviewSharedataTitle1 = 'Review your document data';
export const reviewSharedataTitle2 = 'Review your Unemployment certificate data to be shared with BNP Paribas.';
export const checkSharedataTitle1 = 'Consent document transfer';
export const checkSharedataTitle2 = 'You are about to fetch your Unemployment certificate from Arbetsförmedlingen.';
export const reviewGetdataBoxTitle = 'Unmployment certificate';
export const reviewGetdataBoxItems = {
  'Employment Status:': '',
  'Employment status start date:': '',
  'Employment certificate request date:': '',
};
export const reviewShareddataBoxItems = {
  'Employment Status:': 'Unemployed',
  'Employment status start date:': '20220420',
  'Employment certificate request date:': '20220428',
};
export const reviewGetdataInfo = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.';
export const reviewGetdataButtonText = 'Continue to get data';
export const reviewGetdataButtonText2 = 'Continue to share data';
export const finishGetdataButtonText = 'Go to share data';
export const finishSharedataButtonText = 'Finish share data';
export const checkGetdataInfo = "BNP Paribas will be provided information that you are registered as a job seeker in Arbetsförmedlingen and the date of such registration. For privacy reasons, BNP Paribas will not have access to any other information about you. You can withdraw your consent at any time which will terminate any further data transfer. When you are no longer registered as a job seeker in Arbetsförmedlingen, the transfer automatically terminates and you will need to provide an additional consent for the 'job-seeker status' final date to be transfered to BNP Paribas.";
export const successGetdata = 'Your Registration certificate from Arbetsförmedlingen has now been fetched. Click view data to review all the fetched data.';
export const checkGetdataCheckInfo = [
  'I give my consent for the Project OAK to handle and transfer this information once to BNP Paribas.',
  'I agree that this process removes the confidentiality of the information I choose to share with BNP Paribas.',
  'I have reviewed the information and confirm that no unwanted information is included in the transfer to Dummy.',
];
export const checkGetdataButtonText = 'Consent and get data';
export const reviewShareButtonText = 'Share later';

export const MissingCertText1 = "We can't find your employement certificate";
export const MissingCertText2 = "Make sure you're registeted at Arbetsförmedligen as unemployed.";
export const MissingCertTextp = "When you've registed in Arbetsförmedligen you can always come back to your Project OAK wallet and continue you process.";
export const FetchingDataText = 'Fetching data...';
export const TryAgainlaterText = 'Something went wrong with your transfer. Try again later.';
