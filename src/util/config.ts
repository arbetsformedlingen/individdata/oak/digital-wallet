export default {
  backendBaseUrl: process.env.REACT_APP_BACKEND_BASE_URL || 'https://digital-wallet-backend-oak-develop.test.services.jtech.se/',
  backendWsUrl: process.env.REACT_APP_BACKEND_WS_URL || 'wss://digital-wallet-backend-oak-develop.test.services.jtech.se/',
  idpBaseUrl: process.env.REACT_APP_IDP_BASE_URL || 'https://oak-identity-provider-oak-develop.test.services.jtech.se/',
  podProviderBaseUrl: process.env.REACT_APP_POD_PROVIDER_BASE_URL || 'https://oak-pod-provider-oak-develop.test.services.jtech.se/',
};
